export default function Statement(props){
    return(
        <>
        <div className="statement">
        <h1>{props.id}.</h1><h1 id="definition">{props.name}</h1>
        </div>
        </>
    );
}