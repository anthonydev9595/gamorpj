import React, {useState, useEffect}from "react";

function Time({hour}){
    return(
    <> 
    <div className="clock">
    <h3>{hour}</h3>
    </div>
    </>
    )
}

export default function Clock(){
    const[hour, setHour] = useState(new Date().toLocaleTimeString());

    useEffect(()=>{
        let temp;
        temp = setInterval(()=>{
            setHour(new Date().toLocaleTimeString());
        },1000);
    })

    return(
        <>
        <Time hour= {hour}></Time>
        </>
    )
}