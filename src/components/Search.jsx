import React, { useState, useEffect } from 'react';

function Game({ name }) {
    return (
        <p>{name}</p>
    );
}

export default function Search() {
    const [games, setGames] = useState([]);

    useEffect(() => {
        const getGames = async (url) => {
            let res = await fetch(url),
                json = await res.json();
            json.results.forEach(async (el) => {
                let res = await fetch(el.url),
                    json = await res.json();

                let game = {
                    name: json.name,
                };

                setGames((games) => [...games, game])

            });

        }
        getGames("https://api.twitch.tv/helix/games");

    }, []);

    return (
        <>
            <h2>{name}</h2>
            {games.length === 0 ? (
                <h3>Loading...</h3>
            ) : (
                games.map((el) => (
                    <Game name={el.name} />
                ))
            )}
        </>
    );

}