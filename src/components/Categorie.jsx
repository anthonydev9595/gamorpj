export default function Categorie(props){
    return(
        <>
        <div className={props.class}>
            <h4>/{props.id}</h4>
            <h3>{props.name}</h3>
            &#10141;
        </div>
        </>
    );
}