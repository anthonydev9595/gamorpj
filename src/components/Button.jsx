export default function Button(props){
    return (
        <button type="button" className={props.class} id={props.id}><a href={props.url} id={props.id}>{props.name}</a></button>
    );
}