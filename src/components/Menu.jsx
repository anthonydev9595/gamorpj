import MenuButton from "./MenuButton";
import Home from "../assets/Home-purple.png";

export default function Menu(){
    return(
        <>
        <a className="home-logo" href="/"></a>
        <MenuButton name="Stream"></MenuButton>
        <MenuButton name="Party"></MenuButton>
        <MenuButton name="Premium"></MenuButton>
        </>
    );
}