export default function MenuButton(props) {
    return <a className={props.class} href={props.src} id={props.id}> {props.name} </a>;
}
