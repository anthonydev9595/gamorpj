import { useState } from "react";
import './formApp.css'

function formApp() {
    const [form, setForm] = useState({});

    const handleChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        alert("Submited");
    }

    return (
        <>
            <div className="container">
                <h2>Sign in:</h2>
                <form onSubmit={handleSubmit}>
                    <label htmlFor="user">User:</label>
                    <input type="text" id="user" name="user" value={form.user} onChange={handleChange} />

                    <label htmlFor="email">Email:</label>
                    <input type="email" id="email" name="email" value={form.email} onChange={handleChange} />

                    <input type="submit" value="Sing in"/>
                </form>

            </div>
        </>
    );
}

export default formApp