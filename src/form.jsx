import React from 'react'
import ReactDOM from 'react-dom/client'
import FormApp from './formApp.jsx'
import './form.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <FormApp />
  </React.StrictMode>,
)