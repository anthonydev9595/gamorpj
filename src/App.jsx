import { useState } from 'react'
import './App.css'
import Button from './components/Button'
import Menu from './components/Menu'
import Clock from './components/Clock'
import Statement from './components/Statement'
import Search from './components/Search'
import Categorie from './components/Categorie'


function App() {

  const [theme, setTheme] = useState('light');
  const toggleTheme = () => theme === 'light' ? setTheme('dark') : setTheme('light');

  return (
    <>
    <div className={theme}>
      <header>
        <Menu></Menu>
        <button className="theme-button" onClick={toggleTheme}><a></a></button>
        <div id="logo"></div>
        <Button name="Sign in" id="sign" url="/form.html"></Button>
        <Button name="Create Account" id="create"></Button>
      </header>
      <section className='container'>
        <card>
          <div className="BG">
            <h1>start</h1>
            <h1 className="org">streaming</h1>
            <h1>games</h1>
            <h1>differently</h1>
          </div>
          <p>gamor now has <a className="bbg"><b>stream party</b></a> plataform</p>
          <Button name="Create Account" id="create"></Button>
          <Button name="Sign in" id="sign" url="/form.html"></Button>
        </card>
        <card className="dif">
          <h1>Fornite New Season</h1>
          <p><b>Join Live Stream</b></p>
          <Clock></Clock>
          <div id="img"></div>
        </card>
        <card>
          <Statement id="01" name=" Choose Platform"></Statement>
          <div className="choices">
            <Button name="🎉 Party" className="choice"></Button>
            <Button name="Matchs" className="choice"></Button>
            <Button name="Streams" className="choice"></Button>
          </div>
          <Statement id="02" name=" Searching Game"></Statement>
          <div className='search-box'>
            <Search></Search>
            <hr />
            <Search></Search>
          </div>
        </card>
      </section>
      <h1 className="trend">Trending Categories</h1>
      <section className='categories'>
        <Categorie class="action" id="01" name="Action Games"></Categorie>
        <Categorie class="sports" id="02" name="Sports Games"></Categorie>
        <Categorie class="adventure" id="03" name="adventure Games"></Categorie>
        <Categorie class="arcade" id="04" name="Arcade Games"></Categorie>
        <Categorie class="fantasy" id="05" name="Fantasy Game"></Categorie>
        <Categorie class="strategy" id="06" name="Strategy Game"></Categorie>
        <Categorie class="shooter" id="07" name="Shooter Games"></Categorie>
        <Categorie class="all" id="VIEW ALL" name="All Categories"></Categorie>
      </section>
      </div>
    </>
  )
}

export default App
